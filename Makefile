ICPX_FLAGS=--std=c++20 -O3 -Xsv -fsycl -qactypes -fintelfpga -fp-model=precise

prepare:
	mkdir -p bin
	mkdir -p out

dbg: prepare
	icpx $(ICPX_FLAGS) --std=c++20 -O0 -g -I./lib/include/ -I./src/ ./lib/*.cpp ./src/*.cpp -o ./bin/test.exe

build: prepare
	icpx $(ICPX_FLAGS) -I./lib/include/ -I./src/ ./lib/*.cpp ./src/*.cpp -o ./bin/test.exe

emu: prepare
	icpx $(ICPX_FLAGS) -DFPGA_EMULATOR -I./lib/include/ -I./src/ ./lib/*.cpp ./src/*.cpp -o ./bin/test.fpga_emu

report: prepare
	icpx $(ICPX_FLAGS) -fsycl-device-code-split=off -Xshardware -Xsboard=ofs_ia840fr0_usm -Xssave-temps -fsycl-link -I./lib/include/ -I./src/ ./lib/*.cpp ./src/*.cpp -o ./bin/test.report

hw: prepare
	icpx $(ICPX_FLAGS) -Xshardware -DFPGA_HARDWARE -fsycl-device-code-split=off -Xsprofile -Xssave-temps -reuse-exe=./bin/test.fpga \
	 -Xsoutput-report-folder=./bin/test.prj -Xsboard=ofs_ia840fr0_usm -o ./bin/test.fpga -I./lib/include/ -I./src/ ./lib/*.cpp ./src/*.cpp

devcloud: prepare
	icpx $(ICPX_FLAGS) -Xshardware -DFPGA_HARDWARE \
	 -fsycl-device-code-split=off -Xsprofile -Xssave-temps -reuse-exe=./bin/test.fpga \
	 -Xsoutput-report-folder=./bin/test.prj -I./lib/include/ -I./src/ ./lib/*.cpp ./src/*.cpp -I./lib/json/single_include \
	 -Xsboard=intel_s10sx_pac:pac_s10_usm -o ./bin/test.fpga

