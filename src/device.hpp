#ifndef DEVICE_H
#define DEVICE_H

#include "constants.hpp"
#include "definitions.hpp"
#include <sycl/sycl.hpp>
// #include <sycl/ext/intel/ac_types/ac_int.hpp>

#if FPGA_HARDWARE || FPGA_EMULATOR || FPGA_SIMULATOR
#include <sycl/ext/intel/fpga_extensions.hpp>
#endif

SYCL_EXTERNAL uint64_t make_8con_mask(const uint64_t cluster);
SYCL_EXTERNAL uint64_t mask_east(const uint64_t cluster);
SYCL_EXTERNAL uint64_t mask_west(const uint64_t cluster);

class populatePixelArrayID;
class clusteringID;
class consumeClustersID;

struct clusterObj
{
    uint32_t candidate;
    uint64_t current_cluster;
    uint32_t x;
    uint32_t y;
    uint32_t n;
    bool keep;
};

struct producerObj{
    std::array<uint32_t,5> pixel_array;
    uint32_t candidate;
    uint32_t sp_word;
};
using producerPipe = sycl::pipe<class producerPipeClass, producerObj>;
using clusterObjPipe = sycl::pipe<class clusterObjPipeClass, clusterObj>;



void populatePixelArray(sycl::queue &q,
                        const uint32_t candidate,
                        const uint32_t *raw_bank,
                        const uint32_t raw_bank_size);

SYCL_EXTERNAL void make_cluster(
    const uint64_t pixel_map,
    const uint64_t starting_pixel,
    const unsigned col_lower_limit,
    const unsigned row_lower_limit,
    clusterObj * retval);

void clusteringKernel(sycl::queue &q);


sycl::event consumeClustersKernel(sycl::queue &q,
                      const uint32_t n_candidates,
                      clusterObj *clusterArray);

void applyGeometryToCluster(const VeloGeometry &g,
                            const clusterObj &cluster,
                            const uint32_t clusterIdx,
                            std::vector<Velo::ClusterElem> &clustersArray);
                            
void no_neighbour_sp(
    const VeloGeometry &g,
    const std::vector<uint8_t> &dev_velo_sp_patterns,
    const std::vector<float> &dev_velo_sp_fx,
    const std::vector<float> &dev_velo_sp_fy,
    const uint32_t clusterOffset,
    uint32_t &clusterCounter,
    std::vector<Velo::ClusterElem> &clustersArray,
    Velo::VeloRawBank<2> const &raw_bank);
#endif // !DEVICE_H