#ifndef HOST_SYCL_H
#define HOST_SYCL_H
#include <sycl/sycl.hpp>

void veloMaskedClusteringRunner(
    sycl::queue &q,
    velo_masked_clustering::Parameters &parameters,
    const unsigned event_start,
    const VeloGeometry *dev_velo_geometry,
    const std::vector<uint8_t> &dev_velo_sp_patterns,
    const std::vector<float> &dev_velo_sp_fx,
    const std::vector<float> &dev_velo_sp_fy,
    std::vector<uint32_t> &clustersCounters,
    std::vector<Velo::ClusterElem> &clustersArray);


#endif // !HOST_SYCL_H
