#include <iostream>
#include "constants.hpp"
#include "definitions.hpp"
#include "parser.hpp"
#include "device.hpp"
#include "host.hpp"
#include <vector>

void veloMaskedClusteringRunner(sycl::queue &q,
                                velo_masked_clustering::Parameters &parameters,
                                const unsigned event_start,
                                const VeloGeometry *dev_velo_geometry,
                                const std::vector<uint8_t> &dev_velo_sp_patterns,
                                const std::vector<float> &dev_velo_sp_fx,
                                const std::vector<float> &dev_velo_sp_fy,
                                std::vector<uint32_t> &clustersCounters,
                                std::vector<Velo::ClusterElem> &clustersArray)
{
    // hardcoded
    const int decoding_version = 2;
    const bool mep_layout = false;
    const unsigned number_of_events = parameters.dev_number_of_events;
    const unsigned event_number = 0; // TODO
    // calculating offsets
    unsigned number_of_candidates = parameters.dev_module_pair_candidate_num[event_number];
    const unsigned cluster_start_offset = event_number * Velo::Constants::n_module_pairs;
    const unsigned candidate_offset = parameters.dev_candidates_offsets[event_number];
    unsigned cluster_num_offset = event_number * Velo::Constants::n_module_pairs;
    const unsigned estimated_number_of_clusters =
        parameters.dev_offsets_estimated_input_size[Velo::Constants::n_module_pairs * number_of_events];

    clustersArray.resize(estimated_number_of_clusters);

    // // Load Velo geometry (assume it is the same for all events)
    const VeloGeometry &g = *dev_velo_geometry;
    const auto velo_raw_event = Velo::RawEvent<decoding_version, mep_layout>{parameters.dev_velo_raw_input.data(),
                                                                             parameters.dev_velo_raw_input_offsets.data(),
                                                                             parameters.dev_velo_raw_input_sizes.data(),
                                                                             parameters.dev_velo_raw_input_types.data(),
                                                                             event_number + event_start};
    std::cout << "run no neighbour sp kernel " << std::endl;
    std::cout << "raw banks: " << velo_raw_event.number_of_raw_banks() << std::endl;
    // // process no neighbour sp
    for (unsigned raw_bank_number = 0; raw_bank_number < velo_raw_event.number_of_raw_banks();
         raw_bank_number++)
    {
        const auto raw_bank = velo_raw_event.raw_bank(raw_bank_number);

        if (raw_bank.type != 63 /*LHCb::RawBank::VP*/ && raw_bank.type != 8 /*LHCb::RawBank::Velo*/)
            continue;

        unsigned module_pair_number = raw_bank.sensor_pair() / 8;
        const unsigned clusterOffset = parameters.dev_offsets_estimated_input_size[module_pair_number + cluster_start_offset];
        unsigned &clusterCounter = clustersCounters[module_pair_number + cluster_num_offset];
        no_neighbour_sp(g, dev_velo_sp_patterns, dev_velo_sp_fx, dev_velo_sp_fy, clusterOffset, clusterCounter, clustersArray, raw_bank);
    }

    std::cout << "run rest of clusters kernel" << std::endl;
    std::cout << "geom size " << sizeof(VeloGeometry) << std::endl;
    clusterObj *rawClusterArray = sycl::malloc_host<clusterObj>(number_of_candidates, q);
    for (size_t i = 0; i < number_of_candidates; i++)
    {
        rawClusterArray[i] = {};
    }
    uint32_t *raw_bank_buf = sycl::malloc_host<uint32_t>(FPGA::max_raw_bank_words, q);

    // Start clustering kernel
    clusteringKernel(q); // it has two pipes (in and out)
    auto consumeEvent = consumeClustersKernel(q, number_of_candidates, rawClusterArray);

    size_t total_banks_words = 0;
    for (unsigned candidate_number = 0; candidate_number < number_of_candidates;
         candidate_number++)
    {
        const uint32_t candidate = parameters.dev_cluster_candidates[candidate_number + candidate_offset];
        uint8_t sensor_number = (candidate >> 3) & 0xFF;
        const auto module_pair_number = sensor_number / 8;

        assert(sensor_number < Velo::Constants::n_sensors);

        const auto raw_bank_number = parameters.dev_velo_bank_index[sensor_number];
        const auto raw_bank = velo_raw_event.raw_bank(raw_bank_number);

        for (size_t i = 0; i < raw_bank.count; i++)
        {
            raw_bank_buf[i] = raw_bank.word[i];
        }
        assert(raw_bank.count <= FPGA::max_raw_bank_words);
        populatePixelArray(q, candidate, raw_bank_buf, raw_bank.count);
        // rest_of_clusters(q,
        //  g, clusterOffset, candidate, raw_bank_vec, clusterCounter, clustersArray);
    }
    // wait for all the cluster to be computed
    consumeEvent.wait();
    // end FPGA clustering
    for (size_t i = 0; i < number_of_candidates; i++)
    {
        if (rawClusterArray[i].keep)
        {
            uint8_t sensor_number = (rawClusterArray[i].candidate >> 3) & 0xFF;
            const auto module_pair_number = sensor_number / 8;
            const unsigned clusterOffset = parameters.dev_offsets_estimated_input_size[module_pair_number + cluster_start_offset];
            const unsigned cluster_num = clustersCounters[module_pair_number + cluster_num_offset]++;
            const unsigned clusterIdx = clusterOffset + cluster_num;
            applyGeometryToCluster(g, rawClusterArray[i], clusterIdx, clustersArray);
        }
    }
    sycl::free(rawClusterArray, q);
    sycl::free(raw_bank_buf, q);
    std::cout << "Runner finished - total bank words " << total_banks_words << std::endl;
}
