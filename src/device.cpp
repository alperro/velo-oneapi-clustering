#include <iostream>
#include "device.hpp"
#include "utilities.hpp"
#include "definitions.hpp"
#include <array>

SYCL_EXTERNAL uint64_t make_8con_mask(const uint64_t cluster)
{
  return cluster | ((cluster & 0x7FFF7FFF7FFF7FFF) << 1) | ((cluster & 0xFFFEFFFEFFFEFFFE) << 15) | (cluster << 16) |
         ((cluster & 0xFFFCFFFCFFFCFFFC) << 17) | ((cluster & 0xFFFEFFFEFFFEFFFE) >> 1) |
         ((cluster & 0x7FFF7FFF7FFF7FFF) >> 15) | (cluster >> 16) | ((cluster & 0x3FFF3FFF3FFF3FFF) >> 17);
}

/**
 * @brief Makes a connectivity mask to the east of the current cluster.
 */
SYCL_EXTERNAL uint64_t mask_east(const uint64_t cluster)
{
  const auto mask = (cluster >> 48);
  return mask | ((mask & 0x7FFF7FFF7FFF7FFF) << 1) | (mask >> 1);
}

/**
 * @brief Makes a connectivity mask to the west of the current cluster.
 */
SYCL_EXTERNAL uint64_t mask_west(const uint64_t cluster)
{
  const auto mask = (cluster << 48);
  return mask | (mask << 1) | ((mask & 0xFFFEFFFEFFFEFFFE) >> 1);
}

/**
 * @brief Makes a cluster with a pixel map and a starting pixel.
 */
SYCL_EXTERNAL void make_cluster(
    const uint64_t pixel_map,
    const uint64_t starting_pixel,
    const unsigned col_lower_limit,
    const unsigned row_lower_limit,
    clusterObj *retval)
{
  uint64_t current_cluster = 0;
  uint64_t next_cluster = starting_pixel;

  // Extend cluster until the cluster does not update anymore
  while (current_cluster != next_cluster)
  {
    current_cluster = next_cluster;
    next_cluster = pixel_map & make_8con_mask(current_cluster);
  }

  // Fetch the number of pixels in the cluster
  const unsigned n = sycl::popcount(current_cluster);

  // Get the weight of the cluster in x
  const unsigned x = col_lower_limit * n + sycl::popcount(current_cluster & 0x00000000FFFF0000) +
                     sycl::popcount(current_cluster & 0x0000FFFF00000000) * 2 +
                     sycl::popcount(current_cluster & 0xFFFF000000000000) * 3;

  // Get the weight of the cluster in y
  const unsigned y =
      row_lower_limit * n + sycl::popcount(current_cluster & 0x0002000200020002) +
      sycl::popcount(current_cluster & 0x0004000400040004) * 2 + sycl::popcount(current_cluster & 0x0008000800080008) * 3 +
      sycl::popcount(current_cluster & 0x0010001000100010) * 4 + sycl::popcount(current_cluster & 0x0020002000200020) * 5 +
      sycl::popcount(current_cluster & 0x0040004000400040) * 6 + sycl::popcount(current_cluster & 0x0080008000800080) * 7 +
      sycl::popcount(current_cluster & 0x0100010001000100) * 8 + sycl::popcount(current_cluster & 0x0200020002000200) * 9 +
      sycl::popcount(current_cluster & 0x0400040004000400) * 10 + sycl::popcount(current_cluster & 0x0800080008000800) * 11 +
      sycl::popcount(current_cluster & 0x1000100010001000) * 12 + sycl::popcount(current_cluster & 0x2000200020002000) * 13 +
      sycl::popcount(current_cluster & 0x4000400040004000) * 14;

  retval->current_cluster = current_cluster;
  retval->x = x;
  retval->y = y;
  retval->n = n;
}

void populatePixelArray(sycl::queue &q, const uint32_t candidate, const uint32_t *raw_bank, const uint32_t raw_bank_size)
{
  q.submit([&](sycl::handler &h)
           { h.single_task<populatePixelArrayID>([=]()
                                                 {
  sycl::host_ptr<const uint32_t> h_raw_bank(raw_bank);

  [[intel::fpga_register]] producerObj prod_obj_cache;
#pragma unroll
  for (size_t i = 0; i < 5; i++)
  {
    prod_obj_cache.pixel_array[i]= 0;
  }
  [[intel::fpga_memory("BLOCK_RAM")]] std::array<uint32_t, FPGA::max_raw_bank_words> raw_bank_cache;
#pragma unroll
  for (size_t i = 0; i < FPGA::max_raw_bank_words; i++)
  {
    if(i < raw_bank_size){
    raw_bank_cache[i]= h_raw_bank[i];
    }else{
    raw_bank_cache[i]= 0;

    }
  }

  const auto sp_index = candidate >> 11;
  prod_obj_cache.candidate = candidate;
  prod_obj_cache.sp_word = raw_bank_cache[sp_index];
  const uint32_t sp_addr = (prod_obj_cache.sp_word & 0x007FFF00U) >> 8;
  const int32_t sp_row = sp_addr & 0x3FU;
  const int32_t sp_col = sp_addr >> 6;

  // sp limits to load
  const int32_t sp_row_lower_limit = sp_row - 2;
  const int32_t sp_row_upper_limit = sp_row + 1;
  const int32_t sp_col_lower_limit = sp_col - 3;
  const int32_t sp_col_upper_limit = sp_col + 1;

#pragma unroll
  for (unsigned k = 0; k < raw_bank_size; ++k)
  {
    const uint32_t other_sp_word = raw_bank_cache[k];
    const uint32_t other_no_sp_neighbours = other_sp_word & 0x80000000U;

    if (!other_no_sp_neighbours)
    {
      const uint32_t other_sp_addr = (other_sp_word & 0x007FFF00U) >> 8;
      const int32_t other_sp_row = other_sp_addr & 0x3FU;
      const int32_t other_sp_col = (other_sp_addr >> 6);
      const uint8_t other_sp = other_sp_word & 0xFFU;

      if (
          other_sp_row >= sp_row_lower_limit && other_sp_row <= sp_row_upper_limit &&
          other_sp_col >= sp_col_lower_limit && other_sp_col <= sp_col_upper_limit)
      {
        const int relative_row = other_sp_row - sp_row_lower_limit;
        const int relative_col = other_sp_col - sp_col_lower_limit;

        prod_obj_cache.pixel_array[relative_col] |= (other_sp & 0X0F) << (4 * relative_row) | (other_sp & 0XF0)
                                                                                   << (12 + 4 * relative_row);
      }
    }
  }
  producerPipe::write(prod_obj_cache); }); }).wait();
}

void applyGeometryToCluster(const VeloGeometry &g,
                            const clusterObj &cluster,
                            const uint32_t clusterIdx,
                            std::vector<Velo::ClusterElem> &clustersArray)
{
  const auto sensor_number = (cluster.candidate >> 3) & 0xFF;
  const size_t ltg_off = g.n_trans * sensor_number;

  const unsigned cx = cluster.x / cluster.n;
  const unsigned cy = cluster.y / cluster.n;

  const float fx = cluster.x / static_cast<float>(cluster.n) - cx;
  const float fy = cluster.y / static_cast<float>(cluster.n) - cy;

  // store target (3D point for tracking)
  const uint32_t chip = cx >> VP::ChipColumns_division;
  const unsigned cid = get_channel_id(sensor_number, chip, cx & VP::ChipColumns_mask, cy);

  const float local_x = g.local_x[cx] + fx * g.x_pitch[cx];
  const float local_y = (cy + 0.5f + fy) * Velo::Constants::pixel_size;

  const float gx = g.ltg[0 + ltg_off] * local_x + g.ltg[1 + ltg_off] * local_y + g.ltg[9 + ltg_off];
  const float gy = g.ltg[3 + ltg_off] * local_x + g.ltg[4 + ltg_off] * local_y + g.ltg[10 + ltg_off];
  const float gz = g.ltg[6 + ltg_off] * local_x + g.ltg[7 + ltg_off] * local_y + g.ltg[11 + ltg_off];

  Velo::ClusterElem cluster_obj;
  cluster_obj.id = get_lhcb_id(cid);
  cluster_obj.x = gx;
  cluster_obj.y = gy;
  cluster_obj.z = gz;
  cluster_obj.phi = hit_phi_16(gx, gy);

  clustersArray.at(clusterIdx) = cluster_obj;
}

void saveClusterCandidate(const VeloGeometry &g,
                          const uint32_t sensor_pair,
                          const uint32_t sensor_index,
                          const uint32_t chip,
                          const uint32_t cx,
                          const uint32_t cy,
                          const float fx,
                          const float fy,
                          const uint32_t clusterIdx,
                          std::vector<Velo::ClusterElem> &clustersArray)
{

  const size_t ltg_off = g.n_trans * sensor_pair;
  const unsigned cid = get_channel_id(sensor_index, chip, cx & VP::ChipColumns_mask, cy);

  const float local_x = g.local_x[cx] + fx * g.x_pitch[cx];
  const float local_y = (cy + 0.5f + fy) * Velo::Constants::pixel_size;

  const float gx = g.ltg[0 + ltg_off] * local_x + g.ltg[1 + ltg_off] * local_y + g.ltg[9 + ltg_off];
  const float gy = g.ltg[3 + ltg_off] * local_x + g.ltg[4 + ltg_off] * local_y + g.ltg[10 + ltg_off];
  const float gz = g.ltg[6 + ltg_off] * local_x + g.ltg[7 + ltg_off] * local_y + g.ltg[11 + ltg_off];

  Velo::ClusterElem cluster_obj;
  cluster_obj.id = get_lhcb_id(cid);
  cluster_obj.x = gx;
  cluster_obj.y = gy;
  cluster_obj.z = gz;
  cluster_obj.phi = hit_phi_16(gx, gy);

  clustersArray.at(clusterIdx) = cluster_obj;
}

void clusteringKernel(sycl::queue &q)
{
  q.submit([&](sycl::handler &h)
           { h.single_task<clusteringID>([=]()
                                         {
                                            while(true){
  [[intel::fpga_register]] producerObj prod_obj_cache = producerPipe::read();
  // Pixel array is now populated
  // Work with candidate on starting_pixel_location
  const auto sp_index = prod_obj_cache.candidate >> 11;
  const auto starting_pixel_location = prod_obj_cache.candidate & 0x7;

  const uint32_t sp_addr = (prod_obj_cache.sp_word & 0x007FFF00U) >> 8;
  const int32_t sp_row = sp_addr & 0x3FU;
  const int32_t sp_col = sp_addr >> 6;
  // limits
  const int32_t row_lower_limit = (sp_row - 2) * 4;
  const int32_t col_lower_limit = (sp_col - 3) * 2;

  const auto sp_relative_row = starting_pixel_location & 0x3;
  const auto sp_relative_col = starting_pixel_location < 4;

  const uint32_t col = sp_col * 2 + sp_relative_col;

  // Work with a 64-bit number
  const uint64_t starting_pixel = ((uint64_t)(0x01 << (11 - sp_relative_row)) << (16 * (col & 0x01))) << 32;
  const uint64_t pixel_map = (((uint64_t)prod_obj_cache.pixel_array[3]) << 32) | prod_obj_cache.pixel_array[2];

  // Make cluster with mask clustering method
  [[intel::fpga_register]] clusterObj cluster;
  make_cluster(pixel_map, starting_pixel, col_lower_limit + 4, row_lower_limit, &cluster);

  const uint64_t hits_with_precedence = (mask_east(cluster.current_cluster) & prod_obj_cache.pixel_array[4]) |
                                        (cluster.current_cluster & (starting_pixel ^ -starting_pixel));

  bool keep_cluster = hits_with_precedence == 0 && !sycl::popcount(cluster.current_cluster & 0x8000800080008000) &&
                      !sycl::popcount(cluster.current_cluster & 0x0001000100010001);


  if (keep_cluster)
  {

    // Interrogate if cluster needs to be extended to the west
    if (sycl::popcount(cluster.current_cluster & 0xFFFF))
    {
      const uint64_t west_pixel_map = (((uint64_t)prod_obj_cache.pixel_array[1]) << 32) | prod_obj_cache.pixel_array[0];
      const uint64_t west_start_pixel = mask_west(cluster.current_cluster) & west_pixel_map;

      [[intel::fpga_register]] clusterObj cluster_extension;
      make_cluster(west_pixel_map, west_start_pixel, col_lower_limit, row_lower_limit, &cluster_extension);

       cluster.x += cluster_extension.x;
       cluster.y += cluster_extension.y;
       cluster.n += cluster_extension.n;

      keep_cluster &= !sycl::popcount(cluster_extension.current_cluster & 0x8000800080008000) &&
                      !sycl::popcount(cluster_extension.current_cluster & 0xFFFF) &&
                      !sycl::popcount(cluster.current_cluster & 0x0001000100010001);
    }
  }
  cluster.keep = keep_cluster;
  cluster.candidate = prod_obj_cache.candidate;
  clusterObjPipe::write(cluster);} }); });
}

sycl::event consumeClustersKernel(sycl::queue &q,
                     const uint32_t n_candidates,
                     clusterObj *clusterArray)
{
  return q.submit([&](sycl::handler &h)
           { h.single_task<consumeClustersID>([=]()
                                              {
  
  sycl::host_ptr<clusterObj> h_cluster_arr(clusterArray);
  [[intel::fpga_memory("BLOCK_RAM")]] std::array<clusterObj,FPGA::batch_transfer_size> cluster_cache = {};

  [[intel::fpga_register]] uint32_t n_clust = 0;
  [[intel::fpga_register]] uint32_t wr_offset = 0;
  while(n_clust < n_candidates){
    [[intel::fpga_register]] uint16_t n_elems = 0;
    while(n_elems < FPGA::batch_transfer_size && n_clust < n_candidates){
      [[intel::fpga_register]] clusterObj cluster = clusterObjPipe::read();
      cluster_cache[n_elems] = cluster;
      n_elems ++;
      n_clust ++;
    }
#pragma unroll
    for (size_t i = 0; i < FPGA::batch_transfer_size; i++)
    {
      h_cluster_arr[i + wr_offset] = cluster_cache[i];
    }
    wr_offset = n_clust;
  } }); });
}

void no_neighbour_sp(
    const VeloGeometry &g,
    const std::vector<uint8_t> &dev_velo_sp_patterns,
    const std::vector<float> &dev_velo_sp_fx,
    const std::vector<float> &dev_velo_sp_fy,
    const uint32_t clusterOffset,
    uint32_t &clusterCounter,
    std::vector<Velo::ClusterElem> &clustersArray,
    Velo::VeloRawBank<2> const &raw_bank)
{
  // need to double up as two indedependet sensors in the bank
  const float *ltg0;
  uint32_t n_sp;

  n_sp = raw_bank.count;

  for (unsigned sp_index = 0; sp_index < n_sp; ++sp_index)
  {
    // Decode sp
    const uint32_t sp_word = raw_bank.word[sp_index];
    const uint32_t sp_addr = (sp_word & 0x007FFF00U) >> 8;
    const uint32_t no_sp_neighbours = sp_word & 0x80000000U;
    uint32_t sensor_index;

    sensor_index = raw_bank.sourceID;

    // There are no neighbours, so compute the number of pixels of this superpixel
    if (no_sp_neighbours)
    {
      // Look up pre-generated patterns
      const int32_t sp_row = sp_addr & 0x3FU;
      const int32_t sp_col = (sp_addr >> 6);
      const uint8_t sp = sp_word & 0xFFU;

      const uint32_t idx = dev_velo_sp_patterns[sp];
      const uint32_t chip = sp_col >> (VP::ChipColumns_division - 1);

      {
        // there is always at least one cluster in the super
        // pixel. look up the pattern and add it.
        const uint32_t row = idx & 0x03U;
        const uint32_t col = (idx >> 2) & 1;
        const uint32_t cx = sp_col * 2 + col;
        const uint32_t cy = sp_row * 4 + row;

        const float fx = dev_velo_sp_fx[sp * 2];
        const float fy = dev_velo_sp_fy[sp * 2];
        const unsigned clusterNum = clusterCounter++;
        const auto cluster_index = clusterOffset + clusterNum;

        saveClusterCandidate(g, raw_bank.sensor_pair(), sensor_index, chip, cx, cy, fx, fy, cluster_index, clustersArray);
      }

      // if there is a second cluster for this pattern
      // add it as well.
      if (idx & 8)
      {
        const uint32_t row = (idx >> 4) & 3;
        const uint32_t col = (idx >> 6) & 1;
        const uint32_t cx = sp_col * 2 + col;
        const uint32_t cy = sp_row * 4 + row;

        const float fx = dev_velo_sp_fx[sp * 2 + 1];
        const float fy = dev_velo_sp_fy[sp * 2 + 1];
        const unsigned clusterNum = clusterCounter++;
        const auto cluster_index = clusterOffset + clusterNum;
        saveClusterCandidate(g, raw_bank.sensor_pair(), sensor_index, chip, cx, cy, fx, fy, cluster_index, clustersArray);
      }
    }
  }
}
