#pragma once

#include "constants.hpp"
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <memory>
#include <array>

namespace Allen
{
  namespace detail
  {
    template <typename R>
    inline R const *offsets_to_content(unsigned int const *offsets, unsigned const event)
    {
      auto const offset = offsets[event];
      return reinterpret_cast<R const *>(offsets) + offset;
    }
  } // namespace detail

  inline unsigned short const *bank_sizes(unsigned int const *sizes, unsigned const event)
  {
    return detail::offsets_to_content<unsigned short>(sizes, event);
  }

  inline unsigned short
  bank_size(unsigned int const *sizes, unsigned const event, unsigned const bank)
  {
    return bank_sizes(sizes, event)[bank];
  }

  inline unsigned char const *bank_types(unsigned int const *types_offsets, unsigned const event)
  {
    return detail::offsets_to_content<unsigned char>(types_offsets, event);
  }

  inline unsigned char
  bank_type(unsigned int const *types_offsets, unsigned const event, unsigned const bank)
  {
    return bank_types(types_offsets, event)[bank];
  }

  // static constexpr uint8_t LastBankType = static_cast<uint8_t>(to_integral(LHCb::RawBank::LastType));
} // namespace Allen

namespace Velo
{

  /**
   * @brief Structure to access VELO clusters.
   */
  template <typename T>
  struct Clusters_t
  {
  protected:
    using half_t = float;
    half_t *m_base_pointer;
    unsigned m_total_number_of_hits;
    unsigned m_offset;

  public:
    constexpr static unsigned element_size = sizeof(unsigned) + sizeof(int16_t) + 3 * sizeof(half_t);
    constexpr static unsigned offset_coordinates = sizeof(unsigned) / sizeof(half_t);
    constexpr static unsigned offset_phi = (sizeof(unsigned) + 3 * sizeof(half_t)) / sizeof(int16_t);

    Clusters_t() = default;
    Clusters_t(const Clusters_t &) = default;
    Clusters_t &operator=(const Clusters_t &) = default;

    Clusters_t(T *base_pointer, const unsigned total_estimated_number_of_clusters, const unsigned offset = 0) : m_base_pointer(reinterpret_cast<half_t *>(base_pointer)),
                                                                                                                m_total_number_of_hits(total_estimated_number_of_clusters), m_offset(offset)
    {
    }

    unsigned id(const unsigned index) const
    {
      assert(m_offset + index < m_total_number_of_hits);
      return reinterpret_cast<unsigned *>(m_base_pointer)[m_offset + index];
    }

    void set_id(const unsigned index, const unsigned value)
    {
      assert(m_offset + index < m_total_number_of_hits);
      reinterpret_cast<unsigned *>(m_base_pointer)[m_offset + index] = value;
    }

    float x(const unsigned index) const
    {
      assert(m_offset + index < m_total_number_of_hits);
      return static_cast<float>(
          m_base_pointer[offset_coordinates * m_total_number_of_hits + 3 * (m_offset + index)]);
    }

    void set_x(const unsigned index, const half_t value)
    {
      assert(m_offset + index < m_total_number_of_hits);
      m_base_pointer[offset_coordinates * m_total_number_of_hits + 3 * (m_offset + index)] = value;
    }

    float y(const unsigned index) const
    {
      assert(m_offset + index < m_total_number_of_hits);
      return static_cast<float>(
          m_base_pointer[offset_coordinates * m_total_number_of_hits + 3 * (m_offset + index) + 1]);
    }

    void set_y(const unsigned index, const half_t value)
    {
      assert(m_offset + index < m_total_number_of_hits);
      m_base_pointer[offset_coordinates * m_total_number_of_hits + 3 * (m_offset + index) + 1] = value;
    }

    float z(const unsigned index) const
    {
      assert(m_offset + index < m_total_number_of_hits);
      return static_cast<float>(
          m_base_pointer[offset_coordinates * m_total_number_of_hits + 3 * (m_offset + index) + 2]);
    }

    void set_z(const unsigned index, const half_t value)
    {
      assert(m_offset + index < m_total_number_of_hits);
      m_base_pointer[offset_coordinates * m_total_number_of_hits + 3 * (m_offset + index) + 2] = value;
    }

    int16_t phi(const unsigned index) const
    {
      assert(m_offset + index < m_total_number_of_hits);
      return reinterpret_cast<int16_t *>(
          m_base_pointer)[m_total_number_of_hits * offset_phi + m_offset + index];
    }

    void set_phi(const unsigned index, const int16_t value)
    {
      assert(m_offset + index < m_total_number_of_hits);
      reinterpret_cast<int16_t *>(
          m_base_pointer)[m_total_number_of_hits * offset_phi + m_offset + index] = value;
    }

    // Pointer accessor for binary search
    int16_t *phi_begin() const
    {
      return reinterpret_cast<int16_t *>(m_base_pointer) +
             m_total_number_of_hits * offset_phi + m_offset;
    }
  };

  typedef Clusters_t<char> Clusters;

  struct ClusterElem
  {
    unsigned id;
    float x, y, z;
    int16_t phi;
    bool operator==(const ClusterElem &other) const
    { // const-methode
      if (id == other.id && x == other.x && y == other.y && z == other.z && phi == other.phi)
      {
        return true;
      }
      return false;
    }
    bool operator<(const ClusterElem &other) const
    { // const-methode
      return id < other.id;
    }
  };

  template <int decoding_version>
  struct VeloRawBank
  {

    // two sensors per bank so count banks as well as sensors
    uint32_t sourceID = 0;
    uint32_t count = 0;
    uint32_t const *word = nullptr;
    uint16_t size = 0;
    uint16_t type = 0;

    // For MEP format
    VeloRawBank(uint32_t source_id, const char *fragment, uint16_t s, uint8_t t)
    {
      type = t;
      sourceID = source_id;
      uint32_t const *p = reinterpret_cast<uint32_t const *>(fragment);
      if constexpr (decoding_version == 2 || decoding_version == 3)
      {
        count = *p;
        p += 1;
      }
      word = p;
      size = s;
    }

    // For Allen format
    VeloRawBank(const char *raw_bank, uint16_t s, uint8_t t) : VeloRawBank(*reinterpret_cast<uint32_t const *>(raw_bank), raw_bank + sizeof(uint32_t), s, t)
    {
    }

    inline uint32_t sensor_pair() const { return sourceID & 0x1FFU; }

    inline uint32_t sensor_index0() const { return (sourceID & 0x1FFU) << 1; }

    inline uint32_t sensor_index1() const { return ((sourceID & 0x1FFU) << 1) | 0x1; }
  };

  template <unsigned decoding_version>
  struct VeloRawEvent
  {
  private:
    uint32_t m_number_of_raw_banks = 0;
    uint32_t const *m_raw_bank_offset = nullptr;
    uint8_t const *m_raw_bank_types = nullptr;
    uint16_t const *m_raw_bank_sizes = nullptr;
    char const *m_payload = nullptr;

    void initialize(const char *event, const uint16_t *sizes, const unsigned char *types)
    {
      const char *p = event;
      m_number_of_raw_banks = *reinterpret_cast<uint32_t const *>(p);
      p += sizeof(uint32_t);
      m_raw_bank_offset = reinterpret_cast<uint32_t const *>(p);
      p += (m_number_of_raw_banks + 1) * sizeof(uint32_t);
      m_payload = p;
      m_raw_bank_types = types;
      m_raw_bank_sizes = sizes;
    }

  public:
    VeloRawEvent(const char *event, const uint16_t *sizes, const unsigned char *types)
    {
      initialize(event, sizes, types);
    }

    VeloRawEvent(
        const char *dev_velo_raw_input,
        const unsigned *dev_velo_raw_input_offsets,
        const unsigned *dev_velo_raw_input_sizes,
        const unsigned *dev_velo_raw_input_types,
        const unsigned event_number)
    {
      initialize(
          dev_velo_raw_input + dev_velo_raw_input_offsets[event_number],
          Allen::bank_sizes(dev_velo_raw_input_sizes, event_number),
          Allen::bank_types(dev_velo_raw_input_types, event_number));
    }

    unsigned number_of_raw_banks() const { return m_number_of_raw_banks; }

    VeloRawBank<decoding_version> raw_bank(const unsigned index) const
    {
      return VeloRawBank<decoding_version>{
          m_payload + m_raw_bank_offset[index], m_raw_bank_sizes[index], m_raw_bank_types[index]};
    }
  };

  template <int decoding_version, bool mep_layout>
  using RawEvent = VeloRawEvent<decoding_version>;
} // namespace Velo

/**
 * @brief Velo geometry description typecast.
 */
struct VeloGeometry
{
  size_t n_trans = 0;
  ;
  std::array<float, Velo::Constants::n_modules> module_zs = {};
  std::array<float, Velo::Constants::number_of_sensor_columns> local_x = {};
  std::array<float, Velo::Constants::number_of_sensor_columns> x_pitch = {};
  std::array<float, 12 * Velo::Constants::n_sensors> ltg = {};
};

namespace velo_masked_clustering
{
  struct Parameters
  {
    std::vector<unsigned> host_total_number_of_velo_clusters = {};
    unsigned host_number_of_events = 0;
    int host_raw_bank_version = 0;
    std::vector<char> dev_velo_raw_input = {};
    std::vector<unsigned> dev_velo_raw_input_offsets = {};
    std::vector<unsigned> dev_velo_raw_input_sizes = {};
    std::vector<unsigned> dev_velo_raw_input_types = {};
    std::vector<unsigned> dev_offsets_estimated_input_size = {};
    std::vector<unsigned> dev_module_pair_candidate_num = {};
    std::vector<unsigned> dev_cluster_candidates = {};
    // MASK_INPUT(dev_event_list_t) dev_event_list;
    std::vector<unsigned> dev_candidates_offsets = {};
    unsigned dev_number_of_events = 0;
    std::vector<unsigned> dev_velo_bank_index = {};
  };
}