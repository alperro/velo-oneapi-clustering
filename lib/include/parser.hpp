#ifndef PARSER_H
#define PARSER_H
#include "constants.hpp"
#include "definitions.hpp"

class parser
{
private:
    /* data */
public:
    parser();
    ~parser();
    void parseInput(const char *filename, velo_masked_clustering::Parameters &params);
    void parseGeometry(const char *filename, VeloGeometry &g);
    void writeOutput(const char *filename, const std::vector<uint32_t> &clustersCounters, const std::vector<Velo::ClusterElem> &clustersArray);
    int compareOutput(const char *filename, const std::vector<uint32_t> &clustersCounters, const std::vector<Velo::ClusterElem> &clustersArray);
    int convertToFormattedOutput(const char *infile, const char *outfile);
};

#endif // !PARSER_H