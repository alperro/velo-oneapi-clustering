#ifndef CONSTANT_VELO_H
#define CONSTANT_VELO_H

#include <cstdint>
#include <cstdlib>
#include <vector>

namespace Allen
{
  namespace VPChannelID
  {
    /// Offsets of bitfield channelID
    enum channelIDBits
    {
      rowBits = 0,
      colBits = 8,
      chipBits = 16,
      sensorBits = 18,
      orfyBits = 26,
      orfxBits = 27
    };

    /// Bitmasks for bitfield channelID
    enum channelIDMasks
    {
      rowMask = 0xffL,
      colMask = 0xff00L,
      chipMask = 0x30000L,
      sensorMask = 0x3fc0000L,
      orfyMask = 0x4000000L,
      orfxMask = 0x8000000L
    };
  } // namespace VPChannelID
  namespace constants
  {
    // Do not include cmath or math_constants just for this one constant
    constexpr float pi_f_float = 3.141592654f;
  } // namespace constants
} // namespace Allen

namespace Velo
{
  namespace Constants
  {
    // Detector constants
    static constexpr unsigned n_modules = 52;
    static constexpr unsigned n_module_pairs = n_modules / 2;
    static constexpr unsigned n_sensors_per_module = 4;
    static constexpr unsigned n_sensors = n_modules * n_sensors_per_module;
    static constexpr float z_endVelo = 770; // FIXME_GEOMETRY_HARDCODING

    // Constants for requested storage on device
    static constexpr float max_number_of_tracks_per_cluster = 0.3f; // Maximum one track every three clusters
    static constexpr unsigned minimum_container_size = 10;          // Lower bound for the track container size
    static constexpr unsigned max_track_size = 26;
    static constexpr unsigned max_tracks_to_follow = 2048;

    static constexpr uint32_t number_of_sensor_columns = 768; // FIXME_GEOMETRY_HARDCODING
    static constexpr uint32_t ltg_size = 16 * number_of_sensor_columns;
    static constexpr float pixel_size = 0.055f; // FIXME_GEOMETRY_HARDCODING
    static constexpr unsigned number_of_raw_banks = 208;
  } // namespace Constants

  namespace Tools
  {
    constexpr float max_input_value = Allen::constants::pi_f_float;
    constexpr float max_output_value_i16 = 1 << 15;
    constexpr float convert_factor_i16 = max_output_value_i16 / max_input_value;
  } // namespace Tools
} // namespace Velo

namespace VP
{
  static constexpr unsigned NModules = Velo::Constants::n_modules;
  static constexpr unsigned NSensorsPerModule = 4;
  static constexpr unsigned NSensors = NModules * NSensorsPerModule;
  static constexpr unsigned NChipsPerSensor = 3;
  static constexpr unsigned NRows = 256;
  static constexpr unsigned NColumns = 256;
  static constexpr unsigned NSensorColumns = NColumns * NChipsPerSensor;
  static constexpr unsigned NPixelsPerSensor = NSensorColumns * NRows;
  static constexpr unsigned ChipColumns = 256;
  static constexpr unsigned ChipColumns_division = 8;
  static constexpr unsigned ChipColumns_mask = 0xFF;
  static constexpr double Pitch = 0.055;
} // namespace VP

namespace FPGA {
  static constexpr size_t max_raw_bank_words = 640; // depends on M20K size
  static constexpr size_t pixel_array_dim = 5; //algo constant
  static constexpr size_t batch_transfer_size = 16; //algo constant
}
#endif //  constants_h
