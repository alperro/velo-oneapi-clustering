#ifndef UTILITIES_SYCL_VELO_H
#define UTILITIES_SYCL_VELO_H
#include <vector>
#include <cstdlib>
#include <cmath>
#include "constants.hpp"

inline uint32_t get_channel_id(
    const unsigned sensor,
    const unsigned chip,
    const unsigned col,
    const unsigned row,
    const unsigned orfx = 0,
    const unsigned orfy = 0)
{
  return (orfx << Allen::VPChannelID::orfxBits) | (orfy << Allen::VPChannelID::orfyBits) |
         (sensor << Allen::VPChannelID::sensorBits) | (chip << Allen::VPChannelID::chipBits) |
         (col << Allen::VPChannelID::colBits) | row;
}

inline int32_t get_lhcb_id(const int32_t cid)
{
  return (((unsigned int)0x2 << 28) & 0xf0000000L) | (cid & 0xfffffffL);
  // return lhcb_id::set_detector_type_id(lhcb_id::LHCbIDType::VELO, cid);
}

inline float signselect(const float &s, const float &a, const float &b) { return (s > 0) ? a : b; }

template <typename T>
inline T fast_atan2f(const T &y, const T &x)
{
  // error < 0.07 rad, no 0/0 security
  const T c1 = .25f * Allen::constants::pi_f_float;
  const T c2 = .75f * Allen::constants::pi_f_float;
  const T abs_y = std::abs(y);

  const T x_plus_y = x + abs_y;
  const T x_sub_y = x - abs_y;
  const T y_sub_x = abs_y - x;

  const T nom = signselect(x, x_sub_y, x_plus_y);
  const T den = signselect(x, x_plus_y, y_sub_x);

  const T r = nom / den;
  const T angle = signselect(x, c1, c2) - c1 * r;

  return copysignf(angle, y);
}
/**
 * @brief Calculates the hit phi in a int16 format.
 * @details The range of the atan2 function is mapped onto the range of the int16,
 *          such that for two hit phis in this format, the difference is stable
 *          regardless of the values.
 */
inline int16_t hit_phi_16(const float x, const float y)
{
  // We have to convert the range {-PI, +PI} into {-2^15, (2^15 - 1)} by multiplying
  // by a factor and then reinterpret cast into int16_t.
  const float float_value = fast_atan2f(y, x) * Velo::Tools::convert_factor_i16;
  const int16_t int16_value = static_cast<int16_t>(float_value);
  return int16_value;
}

#endif // !UTILITIES_SYCL_VELO_H
