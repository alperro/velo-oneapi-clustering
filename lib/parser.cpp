#include "parser.hpp"
#include <fstream>
#include <nlohmann/json.hpp>
#include <memory>
#include <algorithm>
using json = nlohmann::json;

parser::parser()
{
}

parser::~parser()
{
}

void parser::parseInput(const char *filename, velo_masked_clustering::Parameters &params)
{
    std::ifstream f(filename);
    json data = json::parse(f);
    params.host_number_of_events = data["host_number_of_events_t"][0];
    params.host_total_number_of_velo_clusters = data["host_total_number_of_velo_clusters_t"].get<std::vector<unsigned>>();
    params.host_raw_bank_version = data["host_raw_bank_version_t"][0];

    params.dev_number_of_events = data["dev_number_of_events_t"][0];
    // raw input
    params.dev_velo_raw_input = data["dev_velo_raw_input_t"].get<std::vector<char>>();
    params.dev_velo_raw_input_offsets = data["dev_velo_raw_input_offsets_t"].get<std::vector<unsigned>>();
    params.dev_velo_raw_input_sizes = data["dev_velo_raw_input_sizes_t"].get<std::vector<unsigned>>();
    params.dev_velo_raw_input_types = data["dev_velo_raw_input_types_t"].get<std::vector<unsigned>>();
    params.dev_offsets_estimated_input_size = data["dev_offsets_estimated_input_size_t"].get<std::vector<unsigned>>();
    // candidates
    params.dev_module_pair_candidate_num = data["dev_module_candidate_num_t"].get<std::vector<unsigned>>();
    params.dev_cluster_candidates = data["dev_cluster_candidates_t"].get<std::vector<unsigned>>();
    params.dev_candidates_offsets = data["dev_candidates_offsets_t"].get<std::vector<unsigned>>();
    params.dev_velo_bank_index = data["dev_velo_bank_index_t"].get<std::vector<unsigned>>();
    // outputs
    // params.dev_module_pair_cluster_num.resize(params.dev_number_of_events * Velo::Constants::n_module_pairs, 0);
    // params.dev_velo_cluster_container.resize(params.host_total_number_of_velo_clusters.at(0) * Velo::Clusters::element_size, 0);
    // params.dev_velo_clusters.resize(params.host_number_of_events);
}

void parser::parseGeometry(const char *filename, VeloGeometry &g)
{
    std::ifstream f(filename);
    json data = json::parse(f);
    g.n_trans = data["velo_geometry_n_trans"];
    std::vector<float> m_ltg = data["velo_geometry_x_ltg"].get<std::vector<float>>();
    std::copy(m_ltg.begin(),m_ltg.end(), g.ltg.begin());
    assert(m_ltg.size() == g.ltg.size());
    std::vector<float> m_module_zs = data["velo_geometry_module_zs"].get<std::vector<float>>();
    std::copy(m_module_zs.begin(),m_module_zs.end(), g.module_zs.begin());
    assert(m_module_zs.size() == g.module_zs.size());
    std::vector<float> m_local_x = data["velo_geometry_local_x"].get<std::vector<float>>();
    std::copy(m_local_x.begin(),m_local_x.end(), g.local_x.begin());
    assert(m_local_x.size() == g.local_x.size());
    std::vector<float> m_x_p = data["velo_geometry_x_pitch"].get<std::vector<float>>();
    std::copy(m_x_p.begin(),m_x_p.end(), g.x_pitch.begin());
    assert(m_x_p.size() == g.x_pitch.size());
}

void parser::writeOutput(const char *filename, const std::vector<uint32_t> &clustersCounters, const std::vector<Velo::ClusterElem> &clustersArray)
{
    std::ofstream f(filename);
    json data;
    auto cluster_array = json::array();
    for(auto it : clustersArray){
        json obj = {{"id",it.id},{"x",it.x},{"y",it.y},{"z",it.z},{"phi",it.phi}};
        cluster_array.push_back(obj);
    }
    data["dev_velo_cluster_container_t_end"] = cluster_array;
    data["dev_module_cluster_num_t_end"] = clustersCounters;

    f << data;
    f.close();
}

int parser::compareOutput(const char *filename, const std::vector<uint32_t> &clustersCounters,  const std::vector<Velo::ClusterElem> &clustersArray)
{
    std::ifstream f(filename);
    json data = json::parse(f);

    std::vector<uint32_t> cpu_velo_cluster_num = data["dev_module_cluster_num_t_end"];
    if(!std::equal(clustersCounters.begin(),clustersCounters.end(),cpu_velo_cluster_num.begin())){
        std::cout<< "NUM CLUSTERS MISMATCH" << std::endl;
        return -3;
    }
    std::vector<Velo::ClusterElem> cpu_velo_cluster_containter;

    for(auto el : data["dev_velo_cluster_container_t_end"]){
        Velo::ClusterElem obj;
        obj.id = el["id"].get<uint32_t>();
        obj.x = el["x"].get<float>();
        obj.y = el["y"].get<float>();
        obj.z = el["z"].get<float>();
        obj.phi = el["phi"].get<int16_t>();
        cpu_velo_cluster_containter.push_back(obj);
    }
    auto clusterCopy = clustersArray;
    std::sort(clusterCopy.begin(),clusterCopy.end(),[](auto a, auto b){return a.id < b.id;});
    std::sort(cpu_velo_cluster_containter.begin(),cpu_velo_cluster_containter.end(),[](auto a, auto b){return a.id < b.id;});

    if(clustersArray.size() != cpu_velo_cluster_containter.size()){
        std::cout <<"SIZE MISMATCH: "<< clustersArray.size() <<" != "<<cpu_velo_cluster_containter.size() << std::endl;
        return -1;
    }
    if(!std::equal(clusterCopy.begin(),clusterCopy.end(),cpu_velo_cluster_containter.begin())){
        std::cout << "CLUSTER CONTAINER MISMATCH" << std::endl;
        return -2;
    }
    return 0;
}

int parser::convertToFormattedOutput(const char *infile, const char *outfile)
{
    std::ifstream fi(infile);
    std::ofstream fo(outfile);
    json dataOut;
    json dataIn = json::parse(fi);

    const unsigned element_size = sizeof(unsigned) + sizeof(int16_t) + 3 * sizeof(float);
    const unsigned offset_coordinates = sizeof(unsigned) / sizeof(float);
    const unsigned offset_phi = (sizeof(unsigned) + 3 * sizeof(float)) / sizeof(int16_t);

    auto cluster_num = dataIn["dev_module_cluster_num_t_end"].get<std::vector<unsigned int>>();
    auto cluster_containter = dataIn["dev_velo_cluster_container_t_end"].get<std::vector<char>>();
    auto container_ptr = reinterpret_cast<float *>(cluster_containter.data());
    auto n_hits = cluster_containter.size() / element_size;
    auto cluster_array = json::array();
    for (auto i = 0; i < n_hits; i++)
    {
        Velo::ClusterElem cluster;
        cluster.id = reinterpret_cast<unsigned *>(container_ptr)[i];
        cluster.x = reinterpret_cast<float *>(container_ptr)[offset_coordinates * n_hits + 3 * (i)];
        cluster.y = reinterpret_cast<float *>(container_ptr)[offset_coordinates * n_hits + 3 * (i) + 1];
        cluster.z = reinterpret_cast<float *>(container_ptr)[offset_coordinates * n_hits + 3 * (i) + 2];
        cluster.phi = reinterpret_cast<int16_t *>(container_ptr)[n_hits * offset_phi + i];
        std::cout<<n_hits * offset_phi + i<< std::endl;
        auto obj = json{{"id", cluster.id}, {"x", cluster.x}, {"y", cluster.y}, {"z", cluster.z}, {"phi", cluster.phi}};
        cluster_array.push_back(obj);
    }
    dataOut["dev_module_cluster_num_t_end"] = cluster_num;
    dataOut["dev_velo_cluster_container_t_end"] = cluster_array;
    
    fo << dataOut;
    fi.close();
    fo.close();
    return 0;
}