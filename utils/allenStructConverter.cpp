#include <iostream>
#include "../lib/include/parser.hpp"

int main(int argc, char const *argv[])
{
    const char * infile = argv[1];
    const char * outfile = argv[2];
    auto p = parser();
    p.convertToFormattedOutput(infile,outfile);
    return 0;
}
