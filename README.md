# VELO oneAPI clustering


This is a SYCL based implementation of the VELO Masked Clustering algorithm developed by Daniel Campora in [Allen]().
## Getting started

This software implements only the clustering part. The rest of Allen has been striped out as much as possible, while retaining the core functionality.
The software has been tested on Agilex 7 (ia840f) and Stratix 10 (s10sx pac) on both local and Intel DevCloud machines.
The development is ongoing, the `master` branch contains the last release tested.
The `only-std` branch is a no-SYCL version used for debug purposes.

Please check `devel-*` branches for the last improvements.

## How to build

### Requisites
This software requires `nlohmann json` library for input/output files.
Intel oneAPI 2023.1 is required for compilation.
Quartus is required for HW bitstream and reports.

### Make
The `Makefile` in the folder provides these targets:
- `emu` FPGA emulation on CPU (no Quartus required). (~seconds)
- `report` Report of FPGA usage, resources, and estimated performance. (~minutes)
- `hw` Generate HW image for the Agilex IA840F board. (~hours)
- `devcloud` Generate HW image for the Stratix 10 PAC. (~hours)
Just do `make hw` to compile.
## Usage
### Requisites
Make sure the FPGA is seen by SYCL and the driver is configured correctly.
Initialize the FPGA with the USM BSP.

### Run
Compiled executables are built in the `bin` folder.
- `test.fpga_emu` is the emulation executable.
- `test.fpga` is the HW executable.
- `test.prj` is the HW project directory.
- `test.report.prj` is the report directory.

To run the executables, just run them from the root directory. (Eg: `./bin/test.fpga`)

## Benchmarking

The HW executable has `VTune` profiler built-in.
To run profiling:
```
vtune -collect fpga-interaction ./bin/test.fpga
```
VTune reports can be expored from a different machine (oneAPI needed) by copying the VTUne folder and using VTune backend.
```
vtune-backend --data-directory=./r000fi
```

## License
This project is under GPLv3 license.